window.onload = function () {
    var submit,
        firstName,
        lastName,
        brthd,
        email,
        pass,
        address,
        firstNameInput,
        lastNameInput,
        brthdInput,
        passInput,
        addressInput,
        firstNameError,
        lastNameError,
        brthdError,
        genderError,
        emailError,
        passError,
        addressError,
        validation,
        inputArr,
        inputArrLength,
        dot = 0,
        a = 0,
        count = 0,
        i;


    validation = (type, input, id, idError, textError) => {
        inputArr = input.split('');
        if (type == "text") {
            for (i = 0; i < inputArr.length; i += 1) {
                if (inputArr[i] == '"' || inputArr[i] == "'") {
                    id.style.borderColor = 'red';
                    idError.innerHTML = textError;
                    count += 1;
                    i = 0;
                }
            }
        } else if (type == "data") {
            // console.log(brthd);
            if (inputArr.length == 0) {
                id.style.borderColor = 'red';
                idError.innerHTML = textError;
                count += 1;
                i = 0;
            }
        } else if (type == "email") {
            for (i = 0; i < inputArr.length; i += 1) {
                if (inputArr[i] == "@") {
                    a += 1
                }
                if (inputArr[i] == ".") {
                    dot += 1;
                }
            }
            if (a != 1 && dot != 1) {
                id.style.borderColor = 'red';
                idError.innerHTML = textError;
                count += 1;
                i = 0;
            }
        }
        if (input == '') {
            id.style.borderColor = 'red';
            idError.innerHTML = "Вы не заполнили поле";
            count += 1;
            i = 0;
        }
    }


    submit = document.getElementById('submit');
    submit.onclick = () => {
        firstName = document.getElementById('firstName').value;
        firstNameInput = document.getElementById('firstName');
        firstNameError = document.getElementById('firstNameError');
        validation("text", firstName, firstNameInput, firstNameError, 'Вы неправильно ввели имя');


        lastName = document.getElementById('lastName').value;
        lastNameInput = document.getElementById('lastName');
        lastNameError = document.getElementById('lastNameError');
        validation("text", lastName, lastNameInput, lastNameError, 'Вы неправильно ввели фамилию');

        brthd = document.getElementById('brthd').value;
        brthdInput = document.getElementById('brthd');
        brthdError = document.getElementById('brthdError');
        validation("data", brthd, brthdInput, brthdError, 'Вы не ввели дату Вашего рождения');

        email = document.getElementById('email').value;
        emailInput = document.getElementById('email');
        emailError = document.getElementById('emailError');
        validation('email', email, emailInput, emailError, 'Это не емейл');
        // // ^ ([a - z0 -9_ -] +\.)* [a - z0 -9_ -] +@[a - z0 -9_ -]+(\.[a - z0 -9_ -] +)*\.[a - z]{ 2, 6 } $


        pass = document.getElementById('pass').value;
        passInput = document.getElementById('pass');
        passError = document.getElementById('passError');
        validation('pass', pass, passInput, passError, 'Данный пароль невозможно использовать');

        address = document.getElementById('address').value;
        addressInput = document.getElementById('address');
        addressError = document.getElementById('addressError');
        validation("text", address, addressInput, addressError, 'Вы неправильно ввели адрес');
        if (count == 0) {
            alert("Validation passed.")
        }
    }
}